// referencia biblioteca express
var express = require('express')
var bodyParser = require('body-parser');
var app = express()
//se mete el fichero de usuarios a una variable
var usuarios = require('./usuarios.json')
var cuentas = require('./cuentas.json')
var comercios = require ('./comercios.json')
var opercome = require ('./opercome.json')
var login = require('./login.json')
//instanciar el servicio FS
// si no va a cambiar nunca se puede meter como constante pero lo hacemos como var
var fs = require('fs')
//incluimos la variable request json
var requestjson = require('request-json')


//quiero que mi aplicacion utilice el bodyParser
app.use(bodyParser.json())

var urlMlabRaiz = "https://api.mlab.com/api/1/databases/dbbancogpi/collections"
// url de MA:
//var urlMlabRaiz = "https://api.mlab.com/api/1/databases/bdbancomapr/collections"
var apiKey = "apiKey=xWDBPIQOurVzga5Y4JYUHFxJKmBcfIn4"
// apiKey de MA:
//var apiKey = "apiKey=4s3VhLTX39KjrLINjZJanMD5aSctw0YX"
var clienteMlab = requestjson.createClient(urlMlabRaiz + "/usuarios?" + apiKey)


//hago una peticion a la url de Mlab cuando me llegue una petición a V5
//tengo que redirigir la llamada porque si no tendría que meter la APIKEY en la URL de invocacion
app.get('/apitechu/v5/usuarios', function(req, res){
  clienteMlab.get('', function(err, resM, body){
     if(!err){
       res.send(body)
     }
     else{
       res.send(err)
     }
  })
})

//a partir de un usuario ID de entrada (por URL) recuperamos sus datos
//solo hay que obtener el nombre y apellido


app.get('/apitechu/v5/usuarios/:id', function(req, res){
  var id = req.params.id
  console.log(id)
  var query = 'q={"id":' + id + '}'
  var filtrado = "&f={'nombre':1, 'apellido':1, '_id':0}"
  clienteMlab = requestjson.createClient(urlMlabRaiz + "/usuarios?" + query + filtrado + "&l=1&" + apiKey)
  clienteMlab.get('', function(err, resM, body){
     if(!err){
       if (body.length > 0)
//devuelve el primer valor del aray si se han encontrado datos en la query
          res.send(body[0])
       else{
       res.status(404).send('usuario no encontrado')
     }
   }
  })
})

//montar el login a partir de la BBDD de MLAB

app.post('/apitechu/v5/login', function(req, res) {
   var email = req.headers.email
   var password = req.headers.password
   var query = 'q={"email":"' + email + '","password":"' + password + '"}'
   clienteMlab = requestjson.createClient(urlMlabRaiz + "/usuarios?" + query + "&l=1&" + apiKey)
   clienteMlab.get('', function(err, resM, body) {
     if (!err) {
       if (body.length == 1){
         clienteMlab = requestjson.createClient(urlMlabRaiz + "/usuarios?")
         var cambio = '{"$set":{"logged":true}}'
         clienteMlab.put('?q={"id": '+ body[0].id + '}&' + apiKey, JSON.parse(cambio), function(errP, resP, bodyP){
             res.send({"login":"ok", "id":body[0].id, "nombre":body[0].nombre, "apellido":body[0].apellido})
           })
       }
       else {
         res.status(404).send('Usuario no encontrado')
       }
     }
   })
})




//hacemos el logout

app.post('/apitechu/v5/logout', function(req, res) {
var id = req.headers.id

var query = 'q={"id":' + id + ', "logged":true}'
 clienteMlab = requestjson.createClient(urlMlabRaiz + "/usuarios?" + query + "&l=1&" + apiKey)
 clienteMlab.get('', function(err, resM, body) {
     if (!err) {
       if (body.length == 1)
       {
           clienteMlab = requestjson.createClient(urlMlabRaiz + "/usuarios")
           var cambio = '{"$set":{"logged":false}}'
           clienteMlab.put('?q={"id": ' + body[0].id + '}&' + apiKey, JSON.parse(cambio), function(errP, resP, bodyP) {
             res.send({"logout":"ok", "id":body[0].id})
           })
       }
       else {
         res.status(200).send('Usuario no logado previamente')
       }
     }
 })
})


// insertamos un nuevo usuario en la BBDD de usuarios en mongo

app.post('/apitechu/v8/usuarios', function(req, res){
  var identificador = 0
  console.log("entro")
  var id = req.headers.id1
  var email = req.headers.email1
  var password = req.headers.password1
  var nombre = req.headers.nombre1
  var apellido = req.headers.apellido1
  var query = 'q={"email":"' + email + '"}'

  clienteMlab = requestjson.createClient(urlMlabRaiz + "/usuarios?" + query + "&l=1&" + apiKey)
  console.log(urlMlabRaiz + "/usuarios?" + query + "&l=1&" + apiKey)

  clienteMlab.get('', function(err, resM, body) {
       if (!err) {
         if (body.length == 1)
         {
               res.status(404).send('Usuario existente')
         }
         else {
           clienteMlab = requestjson.createClient(urlMlabRaiz + "/usuarios?" + apiKey)
           clienteMlab.get('', function(errU, resU, bodyU){
               if (!errU) {
                 identificador = bodyU.length + 1
                 console.log(identificador)
                 var nuevo = {"id":identificador,
                              "nombre":nombre,
                              "apellido":apellido,
                              "email":email,
                              "password":password
                            }
                  clienteMlab = requestjson.createClient(urlMlabRaiz + "/usuarios")
                  clienteMlab.post("?" + apiKey, nuevo, function(errP, resP, bodyP) {
                   if (!errP) {
                     res.status(200).send('alta efectuada correctamente')
                   }
                   else{
                     res.status(404).send('Usuario existente')
                   }
                  })
                 }
                 else {
                   console.log(errU)
                   res.status(404).send('error en la bbdd de usuarios')
                 }
               })
         }
       }
   })
  })


// insertamos un nuevo movimiento en la BBDD de movimientos en mongo

app.post('/apitechu/v9/movimientos', function(req, res){
 var Iban = req.headers.cuenta
 var fecha = req.headers.fecha
 var importe = req.headers.importe
 var divisa = req.headers.divisa
 const ciudad = "Madrid"
 const tipologia = "1"
 parseFloat(importe)
 var y = 0
 var nuevomov = { "id": y,
                  "fecha":req.headers.fecha,
                  "importe":req.headers.importe,
                  "divisa":req.headers.divisa,
                  "city":ciudad,
                  "tipo":tipologia
                }
console.log(nuevomov)
 var arraymov = []
 var arraymongo = []
 var query = 'q={"IBAN":' + Iban + '}'
  clienteMlab = requestjson.createClient(urlMlabRaiz + "/cuenta3?" + query + "&"+ apiKey)

  clienteMlab.get('', function(err, resM, body) {
       if (!err) {
         var mov = body[0].movimientos
         var saldoant = body[0].saldo
         parseFloat(saldoant)
         console.log(saldoant)
         if (parseFloat(saldoant) < parseFloat(importe))
         {
               res.status(404).send('saldo insuficiente en cuenta')
               res.send({"saldo insuficiente  IBAN":body[0].IBAN, "Idcliente":body[0].idcliente, "saldo":body[0].saldo})
         }
         else {
           nuevo_saldo = 0
           parseFloat(nuevo_saldo)
           nuevo_saldo = parseFloat(saldoant) - parseFloat(importe)
           arraymongo.push(body[0].movimientos)
           for (var i = 0; i < mov.length; i++) {
                  arraymov[i] = mov[i]
             }
           y = i
           nuevomov.id = y +1
           arraymov[y] = nuevomov
           var cambio = '{"$set":{"movimientos":' + JSON.stringify(arraymov) + '}}'
           clienteMlab.put('', JSON.parse(cambio),function(errP, resP, bodyP){
             if (!errP){
               var cambio = '{"$set":{"saldo":' + nuevo_saldo + '}}'
//               res.send({"operacion":"Transferencia","Resultado":"SATISFACTORIO"})
//               res.send({"Iban":Iban, "Saldo":nuevo_saldo})
// tiro para adelante con el cambio de movimientos solamente
               clienteMlab.put('', JSON.parse(cambio),function(errC, resC, bodyC){
                 if (!errC){
                   console.log("punto1")
                   var resultado =  { "IBAN": body[0].IBAN,
                                       "saldo":nuevo_saldo
                                    }
                     res.status(200).send(resultado)
                 }
                 else {
                   console.log("punto2")
                     res.status(404).send({"IBAN":body[0].IBAN, "saldo":nuevo_saldo})
                 }
               })
             }
             else{
               console.log("punto3")
               res.status(404).send("error transferencia")
             }
           })
         }
       }
         else{
           console.log("hay error")
           console.log(body)
           res.status(404).send('error al recuperar la cuenta')
       }
     })
})

//a partir de un movimiento obtenemos el detalle del mismo

app.get('/apitechu/v8/detmovim', function(req, res){
  var cuenta = req.headers.cuenta
  var id = req.headers.idmov
  var arraymov = []
  console.log(cuenta)
  console.log(id)
  var query = 'q={"IBAN":' + cuenta + '}'
   clienteMlab = requestjson.createClient(urlMlabRaiz + "/cuenta3?" + query  + "&"+ apiKey)
   clienteMlab.get('', function(err, resM, body) {
     if(!err){
          console.log("registro recuperado de la coleccion de cuentas")
          arraymov = body[0].movimientos
          console.log(arraymov)
          var y = arraymov.length +1
          //ahora vamos a recorrernos el array para ver el movimeinto en detalle
          if (id > y)
          {
            res.status(404).send("movimiento no existe")
          }
          var z = parseFloat(id) -1
          var tipologia = "Transferencia"
          console.log("z"+ z)
          console.log("id" + id)
          if (arraymov[z].tipo == 1){
            tipologia = "Transferencia"
          }
          else{
            if (arraymov[z].tipo == 2){
              tipologia = "Reintegro"
            }
            else {
              if (arraymov[z].tipo == 3){
                tipologia = "Compra"
              }
              else{
                if (arraymov[z].tipo == 4){
                  tipologia = "Recibo"
                }
                else{
                  if (arraymov[z].tipo == 5){
                    tipologia = "Devolucion"
                  }
                }
              }
            }
          }
          var detalleMov = { "numero": id,
                              "importe": arraymov[z].importe,
                              "fecha": arraymov[z].fecha,
                              "divisa":arraymov[z].divisa,
                              "city":arraymov[z].city,
                              "tipo":arraymov[z].tipo
                            }
          res.status(200).send(detalleMov)
        }
      else{
        res.status(404).send("error al recuperar datos")
      }
    })
})








// a partir de una cuenta consulto el detalle: Saldo  y titular

app.get('/apitechu/v8/detcuentas', function(req, res){
  var cuenta = req.headers.cuenta
  console.log(cuenta)
  var query = 'q={"IBAN":' + cuenta + '}'
   clienteMlab = requestjson.createClient(urlMlabRaiz + "/cuenta3?" + query  + "&"+ apiKey)

   clienteMlab.get('', function(err, resM, body) {
     if(!err){
          console.log("registro recuperado de la coleccion de cuentas")
//          console.log(body[0])
          var saldo = body[0].saldo
          console.log("saldo")
          console.log(saldo)
          console.log("titular")
          var titular = body[0].idcliente
          console.log(titular)
//ahora vamos con ID a recuperar el nombre y apellidos del titular
          var query = 'q={"id":' + titular + '}'
          var filtrado = "&f={'nombre':1, 'apellido':1, '_id':0}"
          clienteMlab = requestjson.createClient(urlMlabRaiz + "/usuarios?" + query + filtrado + "&l=1&" + apiKey)
          clienteMlab.get('', function(errU, resU, bodyU){
             if(!errU){
               console.log("recupero los datos asociados al titular")
               console.log(bodyU)
               if (bodyU.length > 0){
                  var nombre = bodyU[0].nombre
                  var apellido = bodyU[0].apellido
                  var divisa = "EUR"
                  console.log(nombre)
                  console.log(apellido)
                  var detalleIban = { "titular": nombre,
                                      "apellido": apellido,
                                      "saldo": saldo,
                                      "divisa":divisa,
                                    }

                  res.send(detalleIban)
                }
                else{
                  console.log("error al recuperar cliente")
                  res.status(404).send('Error en clientela: Usuario titular no encontrado')
                }
              }
               else{
                  console.log("error al recuperar cliente")
                  res.status(404).send('Error en clientela: Usuario titular no encontrado')
                 }

          })

      }
      else{
        console.log("hay error al recuperar la cuenta")
        res.status(404).send('error al recuperar la cuenta')
          }
   })
})

// Consultar movimientos de un IBAN filtrando por fecha

app.get('/apitechu/v8/registros/fecha', function(req, res){
  var cuenta = req.headers.cuenta
  console.log(cuenta)
  var fechadesde = req.headers.fechadesde
  var fechahasta = req.headers.fechahasta
  console.log(fechadesde)
  console.log(fechahasta)
  console.log(cuenta)
  var arraymovimientos = []
  var arraymov = []
  var arraymov1 = []
  var movimientos = []
  var query = 'q={"IBAN":' + cuenta + '}'
  var filter = 'f={"movimientos":1,"_id":0}'
   clienteMlab = requestjson.createClient(urlMlabRaiz + "/cuenta3?" + query  + "&" + filter  + "&"+ apiKey)

   clienteMlab.get('', function(err, resM, body) {
     if(!err){
       console.log("voy a mostrar movimientos 1")
       console.log(body[0].movimientos)
       arraymov = body[0].movimientos
       for (var i = 0; i < arraymov.length; i++){
             console.log(i)
             arraymov1[i] =arraymov[i]
       }
       console.log(arraymov1)
       console.log(arraymov1.length)
       console.log("estoy en segunda fase")
       var id = 0
       var y = 0
       for (var i = 0; i < arraymov1.length; i++){
         console.log("entro bucle")
         if (arraymov1[i].fecha > fechadesde && arraymov1[i].fecha < fechahasta ){
             console.log(arraymov1[i].fecha)
              id = 1
              arraymovimientos[y] = arraymov1[i]
              y = y + 1
              console.log(y)
              console.log(arraymovimientos[i])
         }
       }

    if (id == 0){
      console.log("no recupero movimientos")
      res.send("cuenta sin movimientos asociados entre las fechas seleccionadas")
    }
    else {res.send(arraymovimientos)}

        }
    else{
      res.status(404).send('error al recuperar la cuenta')
    }
   })
 })














// A partir de un cliente devolver sus cuentas
app.get('/apitechu/v5/cuentas', function(req, res){
  var idcliente = req.headers.id
  var query = 'q={"idcliente":' + idcliente + '}'
  var filter = 'f={"IBAN":1,"_id":0}'
  clienteMlab = requestjson.createClient(urlMlabRaiz + "/cuenta3?" + query  + "&" + filter  + "&"+ apiKey)
  console.log(clienteMlab)

  clienteMlab.get('', function(err, resM, body){
     if(!err){
          console.log(body)
          res.send(body)
        }
   })
 })


 app.get('/apitechu/v5/cuentas/id', function(req, res) {
  var idcliente = req.headers.idcliente

  var query = 'q={"idcliente":' + idcliente + '}'
  var filtro = "&f={'IBAN':1, '_id':0}"
  clienteMlab = requestjson.createClient(urlMlabRaiz + "/cuenta3?" + query + filtro + "&l=5&" + apiKey)
  console.log(urlMlabRaiz + "/cuentas?" + query + filtro + "&l=5&" + apiKey)
  clienteMlab.get('', function(err, resM, body) {
    if (!err) {
      res.send(body)
    }
  })
 })


// a partir de un IBAN mostrar sus movimientos

app.get('/apitechu/v5/movimientos', function(req, res){
  var cuenta = req.headers.cuenta
  console.log(cuenta)
  var arraymov = []
  var query = 'q={"IBAN":' + cuenta + '}'
  var filter = 'f={"movimientos":1,"_id":0}'
   clienteMlab = requestjson.createClient(urlMlabRaiz + "/cuenta3?" + query  + "&" + filter  + "&"+ apiKey)

   clienteMlab.get('', function(err, resM, body) {
     if(!err){
          console.log("movimientos")
          console.log(body)
          for (var i = 0; i < body.length; i++){
            console.log(body[i].movimientos)
                arraymov.push(body[i].movimientos)
          }
          console.log(arraymov)
          console.log(body)
          res.send(body)
        }
   })
 })


 // a partir de un IBAN mostrar sus movimientos cuando el saldo es mayor que el de entrada

 app.get('/pruebagui/v7/movimientos', function(req, res){
   var cuenta = req.headers.cuenta
   var saldo = req.headers.saldo
   var arraymov = []
    var query = 'q={"IBAN":' + cuenta + '","saldo":"{ $lt:"' + saldo + '"} } ] }"'
   var filter = 'f={"movimientos":1,"_id":0}'
    clienteMlab = requestjson.createClient(urlMlabRaiz + "/cuentas?" + query  + "&" + filter  + "&"+ apiKey)

    clienteMlab.get('', function(err, resM, body) {
      if(!err){

           res.send(body)
         }
    })
  })
























// declaro variable a la que asigno el puerto
var port = process.env.port || 3000

app.listen(port)
console.log("API escuchando en el puerto"  + port)

console.log("hola mundo")


console.log("segunda prueba nodemon")
//request response
app.get('/apitechu/v1', function(req, res){
  //console.log(req)
  res.send({"mensaje":"bienvenido a mi API"})
})
//quiero que me devuelva la lista de usuarios
//hay que meter el json descargado de mockaroo a la carpeta de borradorproyecto
app.get('/apitechu/v1/usuarios', function(req, res){

  res.send(usuarios)
})
console.log ("prueba 0")

app.post('/apitechu/v2/usuarios', function(req, res){
//  var nuevo = {"first_name":req.headers.first_name,
//              "country":req.headers.country}
var nuevo = req.body

//mete en la variable usuarios la variable nuevo que está pillando del header de la petición del postman
  usuarios.push(nuevo)
  console.log(req.headers)

//conviete el json de la variable usuarios a string en la constante datos
  const datos = JSON.stringify(usuarios)
//escribe en el fichero usuarios-2 a partir de la variable datos en formato utf8
  fs.writeFile("./usuarios.json", datos, "utf8", function(err){
  if (err){
    console.log(err)}
  else {
    console.log("fichero guardado")
  }
  })
  res.send("alta OK")
})


//vamos a hacer un delete
//:id identifica que es un parámetro que el usuario mandará
app.delete('/apitechu/v1/usuarios/:id', function(req, res){
//splice empieza a contar a prtir del indice que te paso, y luego selecciono cuantos elementos selecciono
  usuarios.splice(req.params.id-1, 1)
  res.send("usuario borrado")
})
//
console.log ("prueba 1")

app.post('/apitechu/v1/monstruo/:p1/:p2', function(req,res){
console.log("parametros")
console.log (req.params)
console.log("querystring")
console.log (req.query)
console.log("headers")
console.log (req.headers)
console.log("body")
console.log (req.body)
  res.send("prueba monstruo")
})
console.log ("prueba 2")

app.post('/apitechu/v3/login', function(req, res){
var email = req.headers.email
var password = req.headers.password
var idusuario = 0
  for (var i = 0; i < login.length; i++){
  if (email == login[i].email && password == login[i].password)
     {
      idusuario = login[i].id
      login[i].logged = true
      break;
      }
  }
  if (idusuario !=0)
      res.send({"encontrado":"si", "id":idusuario})
  else    {
    res.send({"encontrado":"no"})
  }
})

app.post('/apitechu/v3/logout', function(req, res){
var email = req.headers.email
var idusuario = 0
var usuario_logado = false

  for (var i = 0; i < login.length; i++){
  if (email == login[i].email && login[i].logged == true)
     {
      idusuario = login[i].id
      usuario_logado = true
      login[i].logged = false
      break;
      }
  }
  if (usuario_logado && idusuario !=0)
      res.send({"usuario desconectado  id":idusuario})
  else {
      res.send("usuario no estaba logado o usuario invalido")
}
})

//LISTADO DE CUENTAS
app.get('/apitechu/v1/cuentas', function(req, res){
//   res.send(cuentas)
  var arraylistado = []
  for (var i = 0; i < cuentas.length; i++) {
    arraylistado.push(cuentas[i].IBAN)
  }
  res.send(arraylistado)
})

//a partir de una cuenta listamos sus movimientos
app.get('/apitechu/v2/cuentas', function(req, res){
  var cuenta = req.headers.cuenta
  var arraymov = []
  var id = 0
  console.log(cuenta)

  for (var i = 0; i < cuentas.length; i++) {
    if (cuentas[i].IBAN == cuenta){
      id = cuentas[i].idcliente
      arraymov = cuentas[i].movimientos
      break
    }
  }
  if (id == 0)
    res.send("cuenta sin movimientos o cuenta erronea")
  else { res.send(arraymov)}
})

//a partir de un usuario damos sus cuentas

app.get('/apitechu/v3/cuentas', function(req, res){
  var cliente = req.headers.cliente
  var arraycuentas = []
  var id = 0

  for (var i = 0; i < cuentas.length; i++) {
    if (cuentas[i].idcliente == cliente){
      id = cuentas[i].idcliente
      arraycuentas.push(cuentas[i].IBAN)
    }
  }
  if (id == 0)
    res.send("usuario sin cuentas")
  else { res.send(arraycuentas)}
})

//  NO CLASE a partir de aquí
//Aplicación de comercios con json
//API en la que a partir de un cliente, le devolvemos los comercios que tiene contratad

app.get('/apiguille/v1/comercios', function(req, res){
  var cliente = req.headers.id
  var arraycomercios = []
  var id = 0
  for (var i = 0; i < comercios.length; i++) {

    if (comercios[i].idcliente == cliente){
         id = comercios[i].idcliente
         arraycomercios.push(comercios[i])
    }
  }
  if (id == 0)
    res.send("usuario sin comercios asociados")
  else {res.send(arraycomercios)}
})

//API en la que a partir de un comercio, le devolvemos todos sus movimientos

app.get('/apiguille/v2/comercios', function(req, res){
  var fuc = req.headers.fuc
  var arraymovimientos = []
  var id = 0
  for (var i = 0; i < opercome.length; i++) {

    if (opercome[i].FUC == fuc){
         id = opercome[i].FUC
         arraymovimientos.push(opercome[i])
    }
  }
  if (id == 0)
    res.send("comercio sin movimientos asociados")
  else {res.send(arraymovimientos)}
})

//API en la que a partir de un comercio, y un rango de fechas, le devolvemos todos sus movimientos entre esas fechas

app.get('/apiguille/v3/comercios', function(req, res){
  var fuc = req.headers.fuc
  var fechadesde = req.headers.fechadesde
  var fechahasta = req.headers.fechahasta
  var arraymovimientos = []
  var id = 0
  for (var i = 0; i < opercome.length; i++) {

    if (opercome[i].FUC == fuc && opercome[i].fecha > fechadesde && opercome[i].fecha < fechahasta ){
         id = opercome[i].FUC
         arraymovimientos.push(opercome[i])
    }
  }
  if (id == 0)
    res.send("comercio sin movimientos asociados")
  else {res.send(arraymovimientos)}
})

//modificar datos de usuario, cambiar email y password de un id
//validaremos que el nuevo email no lo tenga ya ningún otro cliente: sólo lo podría tener un cliente mas

app.post('/apitechu/v8/modusuario', function(req, res) {
   var id = req.headers.id
   var email = req.headers.email
   var password = req.headers.password
   console.log(id)
   console.log(email)
   console.log(password)

   var query = 'q={"email":"' + email + '"}'
   clienteMlab = requestjson.createClient(urlMlabRaiz + "/usuarios?" + query + "&l=1&" + apiKey)
   clienteMlab.get('', function(err, resM, body) {
     if (!err) {
       console.log("Query email ok")
       console.log(body)
       if (body.length == 1 && (body[0].id != id)) {
         console.log(id)
           console.log("email ya existe")
           res.status(404).send('Email ya existe')
       }
       else {
         console.log("email correcto")
         var query = 'q={"id":' + id + '}'
         console.log(id)
         console.log(email)
         console.log(password)
         clienteMlab = requestjson.createClient(urlMlabRaiz + "/usuarios?" + query + "&l=1&" + apiKey)
         clienteMlab.get('', function(err, resM, body) {
           if (!err) {
             console.log("busca id ok")
             if (body.length == 1) {
               console.log("encuentra id para ir a actualizarlo")
               clienteMlab = requestjson.createClient(urlMlabRaiz + "/usuarios?")
               var cambio = '{"$set":{"email":"' + email + '", "password":"' + password + '"}}'
               clienteMlab.put('?q={"id": '+ body[0].id + '}&' + apiKey, JSON.parse(cambio), function(errP, resP, bodyP){
                   res.send({"cambio":"ok", "id":body[0].id, "nombre":body[0].nombre, "apellido":body[0].apellido})
                 })
             }
             else {
               console.log("no encuentra id")
               res.status(404).send('Usuario no encontrado')
             }
           }
         })
       }
     }
    })
})
